var moment = require('moment');
var ping = require('ping');
var fs = require('fs');
var hosts = ['google.com', 'yandex.ru', '192.168.1.100'];
var file = fs.createWriteStream('Ping result ' + moment().format('MMMM Do YYYY, h:mm:ss a'));

file.on('error', function (err) { console.log (err)
});
hosts.forEach(function (host) {
  // WARNING: -i 2 argument may not work in other platform like window
  ping.promise.probe(host, {
    timeout: 10,
    min_reply: 2,
    extra: ["-i 2"]
  }).then(function (res) {
    array = res.output.split('\n');
    index = array.length - 1;
    while (index >= 0) {
      if (array[index].indexOf('PING') > -1 || array[index].indexOf('bytes') > -1) {
        array.splice(index, 1);
      }
      else if (array[index].length < 1) {
        array.splice(index, 1);
      }
      index -= 1;
    }
  }).then (function () {
    array.forEach(function (v) {
      file.write(v + '\n');
    });
  })
});