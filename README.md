#### Install dependencies: ####
* Node.JS 4.x

#### What to do after cloning: ####
```
npm install
```

#### How to run the application ####
Start application:
```
node ping.js
```